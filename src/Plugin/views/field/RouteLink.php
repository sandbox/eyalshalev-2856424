<?php

namespace Drupal\views_route_field\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\views\Plugin\views\field\LinkBase;
use Drupal\views\ResultRow;
use Symfony\Component\Routing\Route;

/**
 * @ViewsField("route_link")
 */
class RouteLink extends LinkBase {

  /**
   * The regex used on route paths to determine the individual route parameters.
   */
  const ROUTE_REGEX = '/(?<=\{)\w*(?<!\})/';

  /**
   * {@inheritdoc}
   */
  protected function getUrlInfo(ResultRow $row) {
    // Tokenize all the parameters.
    $arguments = array_map([$this, 'tokenizeValue'], $this->options['route_options']);
    return Url::fromRoute($this->options['route'], $arguments);
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['route'] = ['default' => NULL];
    $options['route_options'] = ['default' => []];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $options = ($form_state->getValue('options') ? : []) + [
      'route' => $this->options['route'],
      'route_options' => $this->options['route_options'],
    ];
    $form_state->setValue('options', $options);

    $form['route'] = [
      '#type' => 'select',
      '#title' => $this->t('Route'),
      '#options' => $this->getRouteOptions(),
      '#description' => $this->t('Choose a route to use.'),
      '#ajax' => [
        'wrapper' => 'route-options-wrapper',
        'callback' => static::class . '::routeOptionsCallback'
      ],
      '#default_value' => $this->options['route'],
      '#empty_option' => $this->t('- Select -'),
      '#empty_value' => '',
    ];

    $form['route_options'] = static::routeOptionsCallback($form, $form_state);

    return $form;
  }

  /**
   * Callback function for the route_options element.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @return array
   *   The route_options element.
   */
  public static function routeOptionsCallback(array $form, FormStateInterface $form_state) {
    /** @var \Symfony\Cmf\Component\Routing\PagedRouteProviderInterface $route_provider */
    $route_provider = \Drupal::service('router.route_provider');
    $matches = [[]];

    $route_options = [
      '#prefix' => '<div id="route-options-wrapper">',
      '#suffix' => '</div>',
      '#type' => 'fieldset',
      '#title' => t('Route options'),
      '#tree' => TRUE,
    ];

    if (($route_id = $form_state->getValue(['options', 'route']))) {
      $route = $route_provider->getRouteByName($route_id);
      preg_match_all(static::ROUTE_REGEX, $route->getPath(), $matches);

      foreach ($matches[0] as $option) {
        $route_options[$option] = [
          '#type' => 'textfield',
          '#title' => $option,
          '#required' => !$route->hasDefault($option),
          '#default_value' => $form_state->getValue(['options', 'route_options', $option])
        ];
      }
    }

    if (empty($matches[0])) {
      $route_options['#attributes']['class'][] = 'hidden';
    }

    return $route_options;
  }

  /**
   * Returns a list of all the routes ids.
   *
   * @return string[]
   */
  public function getRouteOptions() {
    /** @var \Symfony\Cmf\Component\Routing\PagedRouteProviderInterface $route_provider */
    $route_provider = \Drupal::service('router.route_provider');
    $options = [];

    foreach ($route_provider->getRoutesPaged(0) as $route_name => $route) {
      $options[$route_name] = strpos($route_name, '<') === 0 ? $route_name : $route->getPath();
    }

    return $options;
  }

}